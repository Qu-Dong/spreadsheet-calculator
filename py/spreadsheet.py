import argparse
from sheet import Sheet
from sheet_console import SheetConsole
from sheet_evaluator import SheetEvaluator

EVERYTHING_OK = 0
CYCLE_DETECTED = 1
UNEXPECTED_ERROR = 2


class SpreadSheet:
    def __init__(self, fn=None):
        self.sheetconsole = SheetConsole(fn)
        width, height, lines = self.sheetconsole.read_input()
        self.sheet = Sheet(height, width)
        self.sheet.set_cells(lines)
        self.evaluator = SheetEvaluator(self.sheet)

    def evaluate(self) -> bool:
        return self.evaluator.evaluate()

    def show_sheet(self):
        self.sheetconsole.write_output(self.sheet)


def main(fn=None):
    try:
        spreadsheet = SpreadSheet(fn)
        has_cycle = spreadsheet.evaluate()
        spreadsheet.show_sheet()
    except Exception as e:
        print(e)
        return UNEXPECTED_ERROR

    if has_cycle:
        return CYCLE_DETECTED
    else:
        return EVERYTHING_OK


if __name__ == "__main__":
    parser = argparse.ArgumentParser('Spreadsheet Calculator')
    parser.add_argument('-i', '--input', dest='fn',
                        help='if specified, should be the input file name and output file be be generated; '
                             'otherwise, script will read input from stdin and output result in stdout')
    args = parser.parse_args()
    return_code = main(args.fn)
    exit(return_code)
