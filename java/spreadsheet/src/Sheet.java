import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class Sheet {

    private int height;
    private int width;
    private HashMap<String, List<String>> cells = new HashMap<>();
    private HashMap<String, Float> evaluatedCells = new HashMap<>();
    private boolean hasCycle = false;

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getSize() {
        return height * width;
    }

    public void setSize(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void setHasCycle(boolean hasCycle) {
        this.hasCycle = hasCycle;
    }

    public boolean getHasCycle() {
        return hasCycle;
    }

    private String getCellName(int row, int col) {
        return String.format("%s%s", (char) (64 + row), col);
    }

    public String lineNumToCellName(int lineNum) throws Exception {
        int row = lineNum / width;
        int col = lineNum % width;
        row++;
        col++;
        if (row > height) {
            throw new Exception("line_num exceed the Sheet height");
        }
        return getCellName(row, col);
    }

    public boolean isCell(String cellName) {
        int firstCharAscii = cellName.charAt(0);
        return (firstCharAscii >= 65 && firstCharAscii <= 90);
    }

    public HashMap<String, List<String>> getCells() {
        return cells;
    }

    public void setCells(ArrayDeque<String> lines) throws Exception {
        for (int lineNum = 0; lineNum < getSize(); lineNum++) {
            String cellName = lineNumToCellName(lineNum);
            String cellStr = lines.poll();
            if (cellStr == null) {
                break; // not all cells are filled
            }
            cells.put(cellName, Arrays.asList(cellStr.split(" ")));
        }
    }

    public List<String> getCell(String cellName) {
        return cells.get(cellName);
    }

    public void setCell(String cellName, List<String> cellVal) {
        cells.put(cellName, cellVal);
    }

    public Float getEvaluatedCell(String cellName) {
        return evaluatedCells.getOrDefault(cellName, null);
    }

    public void setEvaluatedCell(String cellName, float cellVal) {
        evaluatedCells.put(cellName, cellVal);
    }

}
