# SpreadShhet Calculator

## Quick Start

The output will show in `stdout`, and for cyclic cells, the original formula will be output as it is.

The return code would be 0 if all cells can be evaluated; and 1 if some cells have cyclic dependencies.

### Python

Python version: Python 3.6 or above

```sh
> cat input1.txt | python spreadsheet.py
3 2
20.00000
20.00000
20.00000
8.66667
3.00000
1.50000
> ehco $?
0
> cat input2.txt | python spreadsheet.py
4 2
20.00000
20.00000
20.00000
B4
8.66667
3.00000
1.50000
A4
> echo $?
1
```

### Java

```sh
> cd java/spreadsheet/src
> javac Spreadsheet.java
> cat input1.txt|java Spreadsheet
3 2
20.00000
20.00000
20.00000
8.66667
3.00000
1.50000
> cat input2.txt|java Spreadsheet
4 2
20.00000
20.00000
20.00000
B4
8.66667
3.00000
1.50000
A4
```

## General Idea

Each cell can be evaluated by using `stack`. However, for cells which depends on
 others, we need to make sure that all of the prerequisites are evaluated first.
  Therefore, we could use `Togological Sorting` to `BFS` from those cells which
  doesn't need any prerequisites. Both the time and space complexity is `O(mn)`.

## About increment or decrement operator

For the example `1 ++ 2 *`, if `++` applies on `1`, the result will be `4`; however, if the `++` applies on `2`, the result will be `3`. Here I'm implementing the logic based on the first situation which is more logically align with the general case. 