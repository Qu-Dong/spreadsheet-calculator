import java.util.*;

public class SheetEvaluator {
    // a hashmap to store prerequisite as the key and its dependencies as a list
    private HashMap<String, List<String>> prerequistes = new HashMap<String, List<String>>();
    // dictionary to store the cell as the key and how many prerequisites it depends on
    private Map<String, Integer> prerequistesNum = new HashMap<String, Integer>();
    // a doubly linked list to store the cells without any dependencies
    private Deque<String> noPrerequistes = new LinkedList<String>();
    private Sheet sheet = new Sheet();

    public SheetEvaluator(Sheet sheet) {
        this.sheet = sheet;
    }

    public void buildGraph() {
        for (Map.Entry<String, List<String>> cell : sheet.getCells().entrySet()) {
            String cellName = cell.getKey();
            List<String> cellValList = cell.getValue();

            boolean hasPre = false;
            for (String cellRef : cellValList) {
                if (!sheet.isCell(cellRef)) {
                    continue;
                }

                if (!prerequistes.containsKey(cellRef)) {
                    prerequistes.put(cellRef, new ArrayList<String>());
                }
                prerequistes.get(cellRef).add(cellName);

                prerequistesNum.put(cellName, prerequistesNum.getOrDefault(cellName, 0) + 1);
                hasPre = true;
            }
            if (!hasPre) {
                noPrerequistes.add(cellName);
            }
        }
    }

    public void evaluate() {
        buildGraph();
        tranverse();
        sheet.setHasCycle(hasCycle());
    }

    public void tranverse() {
        while (!noPrerequistes.isEmpty()) {
            String noPre = noPrerequistes.poll();
            sheet.setEvaluatedCell(noPre, evalRPN(sheet.getCell(noPre)));

            if (!prerequistes.containsKey(noPre)) {
                continue;
            }
            for (String cell : prerequistes.get(noPre)) {
                prerequistesNum.put(cell, prerequistesNum.get(cell) - 1);
                if (prerequistesNum.get(cell) == 0) {
                    noPrerequistes.add(cell);
                }
            }
        }
    }

    private boolean hasCycle() {
        boolean hasCycle = false;
        for (Map.Entry<String, Integer> item : prerequistesNum.entrySet()) {
            Integer inDegree = item.getValue();
            if (inDegree > 0) {
                hasCycle = true;
                break;
            }
        }

        return hasCycle;
    }

    private float evalRPN(List<String> cellVal) {
        if (cellVal.size() == 0) return 0;
        float[] stack = new float[cellVal.size() / 2 + 1];
        int i = 0;
        for (String token : cellVal) {
            switch (token) {
                case "+":
                    stack[i - 2] += stack[i - 1];
                    i--;
                    break;
                case "-":
                    stack[i - 2] -= stack[i - 1];
                    i--;
                    break;
                case "*":
                    stack[i - 2] *= stack[i - 1];
                    i--;
                    break;
                case "/":
                    stack[i - 2] /= stack[i - 1];
                    i--;
                    break;
                case "++":
                    stack[i - 1] += 1;
                    break;
                case "--":
                    stack[i - 1] -= 1;
                    break;
                default:
                    if (sheet.isCell(token)) {
                        stack[i++] = sheet.getEvaluatedCell(token);
                    } else {
                        stack[i++] = Float.parseFloat(token);
                    }
            }
        }
        return stack[0];
    }


}
