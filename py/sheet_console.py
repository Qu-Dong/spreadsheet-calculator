import sys
from typing import Deque
from collections import deque
from pathlib import Path
from sheet import Sheet


class SheetConsoleError(Exception):
    pass


class SheetConsole:
    def __init__(self, fn=None):
        self.fn = fn
        if self.fn:
            if not Path(self.fn).is_file():
                raise SheetConsole(f"{fn} is not a file")

    def read_input(self):
        return self.read_file(self.fn) if self.fn else self.read_stdin()

    def _get_header(self, lines):
        try:
            width, height = map(int, lines[0].split(' '))
        except:
            raise SheetConsoleError("Invalid header format")
        return width, height

    def _validate_input(self, lines):
        if lines:
            width, height = self._get_header(lines)
        else:
            raise SheetConsoleError("No spreadsheet input")
        lines.popleft()
        return width, height, lines

    def read_file(self, fn: str) -> tuple:
        with open(fn, 'r') as f:
            lines = f.readlines()
        lines = deque([x.strip() for x in lines])
        return self._validate_input(lines)

    def read_stdin(self) -> tuple:
        lines = deque()
        for line in sys.stdin:
            lines.append(line.strip())
        return self._validate_input(lines)

    def write_output(self, sheet: Sheet):
        lines = []
        height, width = sheet.shape
        lines.append(f"{width} {height}")

        for line_num in range(sheet.size):
            cell_name = sheet.line_num_to_cell_name(line_num)
            lines.append(' '.join(sheet.get_cell(cell_name)))
        lines = "\n".join(lines)

        if self.fn:
            basename, ext = self.fn.rsplit('.', 1)
            out_fn = f'{basename}_out.{ext}'
            self.write_file(out_fn, lines)
        else:
            self.write_stdout(lines)

    def write_file(self, fn: str, lines: str):
        with open(fn, 'w') as f:
            f.writelines(lines)

    def write_stdout(self, lines):
        print(lines)
