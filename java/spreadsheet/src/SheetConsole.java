import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SheetConsole {

    private ArrayDeque<String> readStdin() {
        List<String> input = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            input.add(scanner.nextLine());
        }
        return new ArrayDeque<>(input);
    }

    public void writeSheet(Sheet sheet) throws Exception {
        ArrayDeque<String> lines = readStdin();
        String spreadSheetShape = lines.getFirst();
        String[] widthHeight = spreadSheetShape.split(" ");
        int width = Integer.parseInt(widthHeight[0]);
        int height = Integer.parseInt(widthHeight[1]);
        sheet.setSize(width, height);
        lines.poll(); // poll the header
        sheet.setCells(lines);
    }

    public void displaySheet(Sheet sheet) throws Exception {
        System.out.println(String.format("%d %d", sheet.getWidth(), sheet.getHeight()));
        for (int lineNum = 0; lineNum < sheet.getSize(); lineNum++) {
            String cellName = sheet.lineNumToCellName(lineNum);
            Float cellVal = sheet.getEvaluatedCell(cellName);
            if (cellVal == null) {
                System.out.println(String.join(" ", sheet.getCell(cellName)));
            } else {
                System.out.println(String.format("%.5f", cellVal));
            }
        }
    }
}
