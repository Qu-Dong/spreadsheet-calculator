from typing import Union, Deque, List
from collections import defaultdict
import string


class SheetError(Exception):
    pass


class Sheet:
    def __init__(self, height: int, width: int):
        self._height = height
        self._width = width
        self._cells = defaultdict(list)

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, height: int):
        self._height = height

    @property
    def width(self) -> int:
        return self._width

    @width.setter
    def width(self, width: int) -> None:
        self._width = width

    @property
    def shape(self) -> tuple:
        return self._height, self._width

    @property
    def size(self):
        return self.width * self.height

    def line_num_to_rowcol(self, line_num: int) -> tuple:
        row, col = divmod(line_num, self._width)
        row += 1
        col += 1
        if row > self._height:
            raise SheetError(f"Current line_num exceed the Sheet height: {col}")
        return row, col

    def rowcol_to_cell_name(self, row: int, col: int) -> str:
        return f"{chr(ord('A')+row-1)}{col}"

    def line_num_to_cell_name(self, line_num):
        row, col = self.line_num_to_rowcol(line_num)
        return self.rowcol_to_cell_name(row, col)

    def is_valid_cell_name(self, cell_name: str):
        return cell_name[0] in string.ascii_uppercase

    def get_cell(self, cell_name: str) -> List[str]:
        return self._cells[cell_name]

    def set_cell(self, cell_name: str, cell_val: Union[str, float, int, list]):
        if isinstance(cell_val, float) or isinstance(cell_val, int):
            cell_val = f"{cell_val:.5f}"

        if isinstance(cell_val, str):
            cell_val = cell_val.split(" ")

        self._cells[cell_name] = cell_val

    def set_cells(self, lines: Deque[str]):
        line_num = 0
        while lines:
            if line_num > self.size:
                raise SheetError("Input exceed Sheet size")
            cell_name = self.line_num_to_cell_name(line_num)
            self.set_cell(cell_name, lines.popleft())
            line_num += 1

    def get_cells(self):
        return self._cells.items()
