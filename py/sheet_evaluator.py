from typing import List
from collections import defaultdict, deque
from sheet import Sheet


class SheetEvaluationError(Exception):
    pass


class SheetEvaluator:
    def __init__(self, sheet: Sheet):
        self.sheet = sheet
        # a dictionary to store prerequisite as the key and its dependencies
        # as a list
        self._prerequistes = defaultdict(list)
        # a dictionary to store the cell as the key and how many prerequisites
        # it depends on
        self._prerequistes_num = defaultdict(int)
        # a doubly linked list to store the cells without any dependencies
        self._no_prerequistes = deque()
        # a dictionary to store the evaluated cells and its value
        self._evaluated = {}

    def evaluate(self):
        self.build_graph()
        has_cycle = self.tranverse()
        for cell_name, cell_val in self._evaluated.items():
            self.sheet.set_cell(cell_name, cell_val)
        return has_cycle

    def _set_cell_reference(self, cell_name, cell_val_lst) -> bool:
        """
        build cell reference and indegree hashmap, return true if cell have
        cell reference; false otherwise
        """
        have_pre = False

        for cell_ref in cell_val_lst:
            if self.sheet.is_valid_cell_name(cell_ref):
                self._prerequistes[cell_ref].append(cell_name)
                self._prerequistes_num[cell_name] += 1
                have_pre = True
        return have_pre

    def build_graph(self):
        for cell_name, cell_val_lst in self.sheet.get_cells():
            if not self._set_cell_reference(cell_name, cell_val_lst):
                self._no_prerequistes.append(cell_name)

    def _get_token_value(self, token: str) -> float:
        if self.sheet.is_valid_cell_name(token):
            if token in self._evaluated:
                return self._evaluated[token]
            else:
                raise SheetEvaluationError(f"cell {c} is not evaluated")
        else:
            return float(token)

    def evaluate_rpn(self, cell_val: List[str]) -> float:
        stack = []
        for token in cell_val:
            if token not in ('+', '-', '*', '/', '++', '--'):
                stack.append(self._get_token_value(token))
            else:
                if token in ('++', '--'):
                    top = stack.pop()
                    top += [-1, 1][token == '++']
                    stack.append(top)
                else:
                    top, sec = stack.pop(), stack.pop()
                    if token == '+':
                        stack.append(sec + top)
                    elif token == '-':
                        stack.append(sec - top)
                    elif token == '*':
                        stack.append(sec * top)
                    else:
                        stack.append(sec / top)
        return stack[0]

    def _have_cycle(self) -> bool:
        return any(x != 0 for x in self._prerequistes_num.values())

    def tranverse(self) -> bool:
        """
        topological sorting,  bfs
        """
        while self._no_prerequistes:
            no_pre = self._no_prerequistes.popleft()
            self._evaluated[no_pre] = self.evaluate_rpn(
                self.sheet.get_cell(no_pre))

            for cell in self._prerequistes[no_pre]:
                self._prerequistes_num[cell] -= 1
                if self._prerequistes_num[cell] == 0:
                    self._no_prerequistes.append(cell)
        return self._have_cycle()
