public class Spreadsheet {


    public static void main(String[] args) throws Exception {
        try {
            Sheet sheet = new Sheet();
            SheetConsole sheetConsole = new SheetConsole();
            SheetEvaluator sheetEvaluator = new SheetEvaluator(sheet);
            sheetConsole.writeSheet(sheet);
            sheetEvaluator.evaluate();
            sheetConsole.displaySheet(sheet);
            if (sheet.getHasCycle()) {
                System.exit(1);
            }
        } catch (Exception e) {
            System.out.println("Error Message: " + e.getMessage());
            System.exit(2);
        }
    }
}
